package br.com.senac.quiz.dao;

import br.com.senac.quiz.model.Usuario;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexao {

    private static final String URL = "jdbc:mysql://localhost:3306/Quiz";
    private static final String USER = "root";
    private static final String PASSWORD = "123456";
    private static final String DRIVER = "com.mysql.jdbc.Driver";

    public static Connection getConnection() {
        Connection connection = null;

        try {
            //carregando o driver do banco na JVM 
            Class.forName(DRIVER);
            System.out.println("Driver carregado....");
            //tento conectar no banco...
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            System.out.println("Conectado ao banco....");

        } catch (ClassNotFoundException ex) {
            System.out.println("Error carregar o driver ...");
        } catch (SQLException ex) {
            System.out.println("Error connectar ao banco ...");
        }
        return connection;
    }

    public static void create() {
        try {

            String query = "create table perguntas ( "
                    + "id int not null auto_increment primary key , "
                    + "pergunta varchar(200) not null ) ; ";

            Connection connection = Conexao.getConnection();

            Statement statement = connection.createStatement();
            statement.execute(query);

            System.out.println("Criei a tabela ...");

            connection.close();

        } catch (SQLException ex) {
            System.out.println("Erro SQL ...");
        }

    }

    public static void select() {

        try {
            Connection connection = Conexao.getConnection();

            String query = "select * from usuario ;";

            Statement statement = connection.createStatement();

            ResultSet rs = statement.executeQuery(query);
            
           
           
            List<Usuario> lista = new ArrayList<>();
            while (rs.next()) {                
                int id = rs.getInt("id") ; 
                String nome = rs.getString("nome"); 
                String senha = rs.getString("senha"); 
                String apelido = rs.getString("apelido"); 
                
                Usuario usuario = new Usuario(id, nome, senha, apelido);
                lista.add(usuario) ; 
                
            }
            
           
            
            
            for(Usuario u : lista){
                System.out.println(u);
            }
            
             connection.close();
            

        } catch (SQLException ex) {
            System.out.println("Erro executar a query ....");
        }

    }

    public static void main(String[] args) {
        //Conexao.getConnection();
        //create();
        select();
    }

}
