package br.com.senac.quiz.dao;

import br.com.senac.quiz.model.Usuario;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;



public class UsuarioDAO implements DAO<Usuario>{

    @Override
     public List<Usuario> getLista() {

        List<Usuario> lista = new ArrayList<>();
        Connection connection = Conexao.getConnection();

        try {
            String query = "select * from usuario ;";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                int id = rs.getInt("id");
                String nome = rs.getString("nome");
                String senha = rs.getString("senha");
                String apelido = rs.getString("apelido");

                Usuario usuario = new Usuario(id, nome, senha, apelido);
                lista.add(usuario);
            }
        } catch (SQLException ex) {
            System.out.println("Erro executar a query ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("erro eo desconectar");
            }
        }

        return lista;

    }

    @Override
    public void inserir(Usuario usuario) {
        
        //abrir 
        Connection connection = Conexao.getConnection() ; 
        try{
            //definir uma query
        String query = "INSERT INTO usuario (nome , senha , apelido) " 
                        +"values ('" + usuario.getNome()  + "' , "
                        + " '" + usuario.getSenha() + "' ,"
                        + " '" + usuario.getApelido() + "' ) ;" ; 
        System.err.println(query);
            //executar a query 
        Statement statement = connection.createStatement() ; 
        statement.executeUpdate(query) ; 
        
        
        }catch(Exception ex){
            System.out.println("Error ao salvar ... ");
        }finally{
            try{
            //fechar a conexao
            connection.close();
            }catch(Exception ex){
                System.out.println("Error ao fechar conexao ... ");
            }
        }
        
        
        
        
    }

    @Override
    public void atualizar(Usuario usuario) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Usuario getPorId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
}
